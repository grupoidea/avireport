<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduccionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produccions', function(Blueprint $table)
		{
            $table->increments('id');
            /*$table->integer('dia');
            $table->integer('week');
            $table->integer('nro_huevos');
            $table->integer('poblacion');
            $table->integer('mortalidad');
            $table->double('kg_alimentos');
            $table->double('conversion_alimenticia');
            $table->double('porcentaje_productividad');
            $table->double('gramo_ave');
            $table->integer('peso_total_huevos');*/
            $table->json('json_control')->nullable();
            $table->integer('week');
            $table->integer('galpon_id')->unsigned();
            $table->foreign('galpon_id')->references('id')->on('galpons')->onDelete('cascade');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produccions');
	}

}
