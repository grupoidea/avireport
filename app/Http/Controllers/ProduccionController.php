<?php namespace AviReport\Http\Controllers;

use AviReport\Entities\Galpon;
use AviReport\Http\Requests;
use AviReport\Http\Controllers\Controller;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Request;

class ProduccionController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$id = Session::get('id');
        $galpon = Galpon::find($id);
        $jsonControl = json_decode($galpon->produccion->json_control);
        $nextUpdate = new \DateTime($jsonControl->last_update);
        $nextUpdate->add(new \DateInterval('P7D'));
        $jsonControl->next_update = $nextUpdate->format('Y-m-d');

        return view('admin.produccion.index',compact('galpon','jsonControl'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function generaPDF($id)
	{

	}
}
