@extends('layout_admin')

@section('content')
    <section class="content-header">
       <p><h2>Galpon</h2> <h1>{{$galpon->code}}</h1></p>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>Analisis de Crecimiento</h3>

                        <p>Control Semanal de Crecimiento</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-institution"></i>
                    </div>
                    <a href="{{route('galpon.show',$id)}}" class="small-box-footer">Acceder <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>Analisis de Produccion</h3>

                        <p>Control Semanal de Produccion</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{route('produccion.index',$id)}}" class="small-box-footer">Acceder<i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
    </section>
    @endsection

    @section('css-content')
            <!-- CSS -->
    @endsection


    @section('js-content')
            <!-- JS -->
@endsection
